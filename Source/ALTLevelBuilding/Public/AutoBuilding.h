// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Components/InstancedStaticMeshComponent.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "AutoBuilding.generated.h"

UCLASS()
class ALTLEVELBUILDING_API AAutoBuilding : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AAutoBuilding();


	UFUNCTION(BlueprintCallable)
		void Build();


	// Properties


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FVector Size = FVector(4, 4, 4);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float CubesPerSecond = 5;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float Spread = 105;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Counter = 0;


	// Components


	UPROPERTY(EditAnywhere)
		UInstancedStaticMeshComponent* InstancedStaticMesh;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//Called when actor is created
	virtual void OnConstruction(const FTransform& Transform);
};
