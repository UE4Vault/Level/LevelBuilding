// Fill out your copyright notice in the Description page of Project Settings.

#include "AutoBuilding.h"


// Sets default values
AAutoBuilding::AAutoBuilding()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	InstancedStaticMesh = CreateDefaultSubobject<UInstancedStaticMeshComponent>(FName("Instanced Static Mesh"));
	InstancedStaticMesh->RegisterComponentWithWorld(GetWorld());
	RootComponent = InstancedStaticMesh;
	ConstructorHelpers::FObjectFinder<UStaticMesh>MeshRef(TEXT("/Engine/BasicShapes/Cube.Cube"));
	InstancedStaticMesh->SetStaticMesh(MeshRef.Object);
}


//Do stoof


void AAutoBuilding::Build()
{
	//Build one block at a time

	int LX = UKismetMathLibrary::FFloor(Size.X);
	int LY = UKismetMathLibrary::FFloor(Size.Y);
	int LZ = UKismetMathLibrary::FFloor(Size.Z);
	FVector LLoc;

	if (LX * LY * LZ - 1 >= Counter)
	{
		LLoc.X = (Counter / LX) - ((Counter / (LX * LY)) * Size.Y);
		LLoc.Y = Counter % LX;
		LLoc.Z = Counter / (LX * LY);

		LLoc = LLoc * Spread;

		InstancedStaticMesh->AddInstance(FTransform(FRotator(0, 0 ,0), LLoc, FVector(1, 1, 1)));
		Counter++;
	}
}


// Called when the game starts or when spawned
void AAutoBuilding::BeginPlay()
{
	Super::BeginPlay();

	InstancedStaticMesh->ClearInstances();
	Counter = 0;
	UKismetSystemLibrary::K2_SetTimer(this, "Build", 1 / CubesPerSecond, true);
	
}

// Called every frame
void AAutoBuilding::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called when actor constructs
void AAutoBuilding::OnConstruction(const FTransform& Transform)
{
	InstancedStaticMesh->ClearInstances();
	Counter = 0;

	int LX = UKismetMathLibrary::FFloor(Size.X);
	int LY = UKismetMathLibrary::FFloor(Size.Y);
	int LZ = UKismetMathLibrary::FFloor(Size.Z);

	for (int i = 0; i < (LX * LY * LZ); i++)
	{
		Build();
	}
}